package vod.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.time.LocalTime;

@Component
public class TimeWindowInterceptor implements HandlerInterceptor {

    private int opening = 7;
    private int closing = 14;

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) throws Exception {

        int currentHour = LocalTime.now().getHour();
        if (currentHour >= opening && currentHour < closing) {
            return true;
        } else {
            response.setStatus(418);
            return false;
        }
    }
}
