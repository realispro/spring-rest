package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import vod.model.Director;
import vod.model.Movie;
import vod.service.MovieService;
import vod.web.dto.MovieDto;
import vod.web.dto.RatingDto;

@Component
@RequiredArgsConstructor
@Slf4j
public class RatingValidator implements Validator {

    private final MovieService movieService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(RatingDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RatingDto dto = (RatingDto) target;

        Movie movie = movieService.getMovieById(dto.getMovieId());
        if (movie != null) {
            log.info("Movie presence confirmed: {}", movie.getTitle());
        } else {
            errors.rejectValue("movieId", "error.movie.missing");
        }
       /* if (dto.getRate() < 0.0f || dto.getRate() > 5.0f) {
            errors.rejectValue("rate", "error.rating.wrongValue");
        }
*/
    }
}
