package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.CinemaDto;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class CinemaController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas") // /cinemas?title=Dro&movieId=1
    ResponseEntity<List<CinemaDto>> getCinemas(
            @RequestParam(value = "movieId", required = false) Integer movieId,
            @RequestHeader(value = "foo", required = false) String foo,
            @RequestHeader Map<String, String> headers,
            @CookieValue(value = "some_cookie", required = false) String someCookie){
        log.info("about to retrieve cinemas");
        log.info("movieId request param: {}", movieId);
        log.info("foo header: {}", foo);
        headers.entrySet().forEach(entry->log.info("header: {} {}", entry.getKey(), entry.getValue()));
        log.info("some cookie: {}", someCookie);

        if(movieId!=null) {
            return getCinemasShowingMovie(movieId);
        }

        List<Cinema> cinemas = cinemaService.getAllCinemas();

        log.info("found {} cinemas", cinemas.size());
        return ResponseEntity.ok(cinemas.stream()
                .map(CinemaDto::fromData)
                .toList());
    }


    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDto> getCinema(@PathVariable("cinemaId") int cinemaId){
        log.info("about to retrieve cinema {}", cinemaId);
        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        if(cinema!=null) {
            return ResponseEntity.status(HttpStatus.OK).body(CinemaDto.fromData(cinema));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<CinemaDto>> getCinemasShowingMovie(@PathVariable int movieId){
        log.info("about to retrieve cinemas showing movie {}", movieId);

        Movie movie = movieService.getMovieById(movieId);
        if(movie==null){
            return ResponseEntity.notFound().build();
        }

        List<Cinema> cinemas = cinemaService.getCinemasByMovie(movie);
        log.info("{} cinemas found showing movie {}", cinemas.size(), movieId);
        return ResponseEntity.ok(cinemas.stream().map(CinemaDto::fromData).toList());
    }



}
