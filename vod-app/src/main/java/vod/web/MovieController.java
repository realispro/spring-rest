package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.model.Movie;
import vod.web.dto.MovieDto;
import vod.service.MovieService;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class MovieController {

    private final MovieService moviesService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;


    @GetMapping("/movies")
    ResponseEntity<List<MovieDto>> getMovies() {
        List<MovieDto> movies = moviesService.getAllMovies().stream()
                .map(MovieDto::new)
                .toList();
        log.info("Found movies: " + movies.size());
        return ResponseEntity.ok(movies);
    }

    @GetMapping("/movies/{movieId}")
    ResponseEntity<MovieDto> getMovie(@PathVariable int movieId) {
        log.info("about to retrieve movie {}", movieId);
        if (movieId == 666) {
            throw new IllegalArgumentException("Boom!");
        }

        Movie movie = moviesService.getMovieById(movieId);
        if (movie != null) {
            return ResponseEntity.ok(new MovieDto(movie));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(
            @Validated @RequestBody MovieDto movieDto,
            Errors errors,
            HttpServletRequest request) {
        log.info("about to add movie {}", movieDto);

        if (movieDto.getTitle().equals("Foo")) {
            throw new IllegalArgumentException("Boom!");
        }

        if (errors.hasErrors()) {
            Locale locale = localeResolver.resolveLocale(request);

            String message = errors.getAllErrors().stream()
                    .map(oe -> messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .reduce("errors:\n", (accu, error) -> accu + error + "\n");

            return ResponseEntity.badRequest().body(message);
        }

        Movie movie = moviesService.addMovie(movieDto.toData());
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(movie.getId())
                .toUri();

        return ResponseEntity
                .created(uri)
                .body(new MovieDto(movie));

    }

}
