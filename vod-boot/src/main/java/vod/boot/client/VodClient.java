package vod.boot.client;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class VodClient {

    public static void main(String[] args) {

        RestTemplate restTemplate = new RestTemplate();

        String serviceUrl = "http://localhost:8080/vod/webapi";

        ResponseEntity<MovieClientDto> responseEntity = restTemplate.exchange(
                serviceUrl + "/movies/" + 1,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                MovieClientDto.class
                );

        log.info("[single] response code: {}, response object: {}",
                responseEntity.getStatusCode(),
                responseEntity.getBody());

        ResponseEntity<MovieClientDto[]> responseEntityArray = restTemplate.exchange(
                serviceUrl + "/movies",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                MovieClientDto[].class
        );

        log.info("[array] response code: {}, response body: {}",
                responseEntityArray.getStatusCode(),
                responseEntityArray.getBody());


    }

    @Data
    static class MovieClientDto{
        private int id;
        private String title;
        private String poster;
        private int directorId;
    }
}
